


Müller-Brown surface
--------------------

.. toctree::

    index

In this tutorial, we are going to show how to locate the local minima and transition states of the Müller-Brown surface using the ``atooms-landscape`` package. You can install the package from pypi: ``pip install atooms-landscape``.

Basins of attraction
~~~~~~~~~~~~~~~~~~~~

Here we identify the basins of attraction of the Müller-Brown surface and compare different minimization methods.

.. code:: python
    :name: plots

    import numpy
    from atooms.landscape.surfaces import muller_brown

    def plot_mb():
        import numpy as np
        import matplotlib.pyplot as plt

        def create_mesh(f):
            from itertools import product
            x = np.arange(-1.5, 1, 0.025)
            y = np.arange(-0.5, 2, 0.025)
            X, Y = np.meshgrid(x, y)
            Z = np.zeros(X.shape)
            mesh_size = range(len(X))
            for i, j in product(mesh_size, mesh_size):
                Z[i][j] = f(np.array([X[i][j], Y[i][j]]))
            return X, Y, Z

        def plot_contour(ax, X, Y, Z):
            ax.contour(X, Y, Z, levels=np.linspace(-140, 100, 20))
            ax.axis('square')
            return ax

        fig, ax = plt.subplots(figsize=(6, 6), frameon=False)
        X, Y, Z = create_mesh(muller_brown.value)
        ax = plot_contour(ax, X, Y, Z)

        return fig, ax

    def add_states(ax):
        """Add transition states and minima to plot"""
        props = dict(alpha=.5, facecolor='white', lw=0.0)
        ax.annotate('M1', (0.623499, 0.028038), bbox=props)
        ax.annotate('M2', (-0.558224, 1.441726), bbox=props)
        ax.annotate('M3', (-0.050011, 0.466694), bbox=props)
        ax.annotate('TS1', (-0.822021, 0.624313), bbox=props)
        ax.annotate('TS2', (0.212487, 0.292988), bbox=props)
        return ax

Plot the Muller-Brown surface

.. code:: python

    import matplotlib.pyplot as plt
    fig, ax = plot_mb()
    ax = add_states(ax)
    fig.savefig(f'images/mb.png', bbox_inches='tight')

.. image:: ./images/mb.png

General purpose functions to locate local minima using:

- steepest descent (SD)

- fast inertial relaxation engine (FIRE)

- eigenvector-following (EF)

- conjugate gradients (CG)

- l-BFGS

We also try a SD+CG method which combines a few steps of SD to converge the square gradient below some threshold and CG to speed up convergence once we are close to the minimum.

.. code:: python
    :name: core

    import numpy
    from scipy.optimize import minimize
    from atooms.landscape.surfaces import muller_brown

    def _test_muller_brown_ef(x0, y0, unstable_modes=-1, freeze_iter=-1):
        from atooms.landscape.methods import ef

        coords = numpy.array([x0, y0])
        db = []
        def store(iteration, coords, *args):
            db.append(coords.copy())    
        res = ef.eigenvector_following(coords,
                                       muller_brown.compute,
                                       muller_brown.normal_modes,
                                       method='ef', max_iter=20000,
                                       trust_radius=0.025,
                                       trust_fixed=True,
                                       unstable_modes=unstable_modes,
                                       freeze_iter=freeze_iter,
                                       min_trust=1e-7, max_trust=10.0,
                                       callback=store
        )
        return db

    def _test_muller_brown_sd(x0, y0):
        from atooms.landscape.methods import sd

        coords = numpy.array([x0, y0])
        db = []
        def store(iteration, coords, *args):
            db.append(coords.copy())
        sd.steepest_descent(coords,
                            muller_brown.compute,
                            dx=1e-5, maxiter=10000000, callback=store)
        return db

    def _test_muller_brown_fire(x0, y0):
        from atooms.landscape.methods import fire

        coords = numpy.array([x0, y0])
        db = []
        def store(iteration, coords, *args):
            db.append(coords.copy())
        res = fire(coords,
                   muller_brown.compute,
                   callback=store)
        return db

    def _test_muller_brown_cg(x0, y0):
        coords = numpy.array([x0, y0])
        db = [[x0, y0]]
        def store(coords):
            db.append(coords.copy())
        res = minimize(muller_brown.value, coords, method='CG',
                       jac=muller_brown.gradient,
                       options={'gtol': 1e-10}, callback=store)
        return db

    def _test_muller_brown_cg_sd(x0, y0, sd_iter=10, sd_dx=1e-4):
        from atooms.landscape.methods import sd

        coords = numpy.array([x0, y0])
        db = [[x0, y0]]
        def store(coords):
            db.append(coords.copy())
        def store_sd(iteration, coords, *args):
            db.append(coords.copy())        
        sd.steepest_descent(coords,
                            muller_brown.compute,
                            dx=sd_dx, gtol=1e3, callback=store_sd)
        res = minimize(muller_brown.value, coords, method='CG',
                       jac=muller_brown.gradient,
                       options={'gtol': 1e-10}, callback=store)
        return db

    def _test_muller_brown_sd_fire(x0, y0):
        from atooms.landscape.methods import sd
        from atooms.landscape.methods import fire

        coords = numpy.array([x0, y0])
        db = [[x0, y0]]
        def store(iteration, coords, *args):
            db.append(coords.copy())        
        sd.steepest_descent(coords,
                            muller_brown.compute,
                            dx=1e-4, gtol=1e4, callback=store)
        res = fire(coords,
                   muller_brown.compute,
                   callback=store)
        return db


    def _test_muller_brown_lbfgs(x0, y0):
        coords = numpy.array([x0, y0])
        db = [[x0, y0]]
        def store(coords):
            db.append(coords.copy())
        res = minimize(muller_brown.value, coords, method='L-BFGS-B',
                       jac=muller_brown.gradient,
                       options={'gtol': 1e-10, 'maxcor': 10}, callback=store)
        return db

We map the basins of attractions of the MB surface, coloring the points according to the basin they belong to:

- M1 basin: green

- M2 basin: blue

- M3 basin: orange

In some cases, the minimization does not converge and we mark these points as crosses.

.. code:: python



    def basins(method='sd', dx=0.1):
        import numpy as np
        from itertools import product
        from atooms.landscape.surfaces import muller_brown
        x = np.arange(-1.5, 1, dx)
        y = np.arange(-0.5, 2, dx)
        X, Y = np.meshgrid(x, y)
        Z = np.zeros(X.shape, dtype=int)
        mesh_size = range(len(X))
        mins = []
        for i, j in product(mesh_size, mesh_size):
            try:
                if method == 'sd':
                    db = _test_muller_brown_sd(X[i][j], Y[i][j])
                elif method == 'cg':
                    db = _test_muller_brown_cg(X[i][j], Y[i][j])
                elif method == 'cg+sd':
                    db = _test_muller_brown_cg_sd(X[i][j], Y[i][j])
                elif method == 'ef':
                    db = _test_muller_brown_ef(X[i][j], Y[i][j], unstable_modes=0)
                elif method == 'lbfgs':
                    db = _test_muller_brown_lbfgs(X[i][j], Y[i][j])
                elif method == 'fire':
                    db = _test_muller_brown_fire(X[i][j], Y[i][j])
                elif method == 'sd+fire':
                    db = _test_muller_brown_sd_fire(X[i][j], Y[i][j])
            except OverflowError:
                Z[i][j] = -1
                continue
        
            # Find minimum of the basin of attraction
            value = muller_brown.value(db[-1])
            f = f'{value:.4f}'
            if f not in mins:
                mins.append(f)
            Z[i][j] = mins.index(f)

        fig, ax = plot_mb()
        for i, j in product(mesh_size, mesh_size):
            if Z[i][j] == -1:
                ax.plot(X[i][j], Y[i][j], marker='x', color='k')
            else:
                ax.plot(X[i][j], Y[i][j], marker='o', color=f'C{Z[i][j]}')
        ax = add_states(ax)
        fig.savefig(f'images/mb_basin_{method}.png', bbox_inches='tight')
    basins('sd+fire')

::

    >>>


.. code:: python

    basins('fire')
    basins('lbfgs')    
    basins('ef')
    basins('sd')
    basins('cg')
    basins('cg+sd')

EF and FIRE reproduce well the basins of attraction obtained by steepest descent (with fixed step size). The borders between the basins are a bit different with FIRE and EF than with SD, but the topology of the basins is respected. Also, they differ only far away from the transition states.

- SD

.. image:: ./images/mb_basin_sd.png

- EF

.. image:: ./images/mb_basin_ef.png

- FIRE

.. image:: ./images/mb_basin_fire.png

On the other hand, CG and l-BFGS sometimes tunnel from one basin to another: the partitioning of the surface into basins of attraction is inconsistent.

- CG

.. image:: ./images/mb_basin_cg.png

- l-BFGS

.. image:: ./images/mb_basin_lbfgs.png

This issue may depend on the specific line search methods implemented by scipy. Others have reported issues with line minimizations in scipy `https://github.com/scipy/scipy/issues/15643 <https://github.com/scipy/scipy/issues/15643>`_.

Minimization pathways
~~~~~~~~~~~~~~~~~~~~~

We trace the minimization paths from

- SD

- EF

- FIRE

- CG

to highlight how, in some cases, CG leads to the wrong basin of attraction.

.. code:: python



    def test_muller_brown():
        # See D. J. Wales, J. Chem. Phys. 101, 3750 (1994)
        min1, min2, min3 = (0.623499, 0.028038), (-0.558224, 1.441726), (-0.050011, 0.466694)
        points_dict = {
            (-0.5, 0.75): min3,  # 3
            (-0.6, 0.9): min2,  # 4
            (-0.05, -0.3): min1,  # 6
            (-0.5, -0.3): min3,  # 7
            (-1.3, 1.6): min2,  # 8
            (0.9, 0.7): min1,  # 10
            (0.5, 1.7): min2,  # 11
            (0.8, 1.1): min3,  # 12
            (0.7, 1.3): min3,  # 13
            (0.6, 1.55): min2,  # 14
            (-1.0, -0.3): min3,  # 15
            (-1.3, -0.1): min2,  # 16
        }
        # Paths reaching the minimum (value) from a starting point (key)
        for i, k in enumerate(points_dict):
            fig, ax = plot_mb()
            ax = add_states(ax)
            ref_coords = points_dict[k]

            db = _test_muller_brown_ef(k[0], k[1], unstable_modes=0)
            xs = numpy.array(db)
            ax.plot(xs[:,0], xs[:,1], '-', color='orange', label='EF')
            ax.plot(ref_coords[0], ref_coords[1], 'ro')

            db = _test_muller_brown_sd(k[0], k[1])
            xs = numpy.array(db)
            ax.plot(xs[:,0], xs[:,1], '-', color='red', label='SD')

            db = _test_muller_brown_cg(k[0], k[1])
            xs = numpy.array(db)
            ax.plot(xs[:,0], xs[:,1], linestyle='--', marker='o', color='blue', label='CG')

            db = _test_muller_brown_sd_fire(k[0], k[1])
            xs = numpy.array(db)
            ax.plot(xs[:,0], xs[:,1], linestyle='--', marker='o', color='cyan', label='SD+FIRE')

            db = _test_muller_brown_fire(k[0], k[1])
            xs = numpy.array(db)
            ax.plot(xs[:,0], xs[:,1], '-', color='purple', label='FIRE')
        
            fig.legend()
            fig.savefig(f'images/mb_path_{i}.png')
            
    test_muller_brown()

The paths of CG lead to other local minima in some cases, while EF, SD, FIRE always converge to the same minimum for this set of initial conditions.

Path 0:

.. image:: ./images/mb_path_0.png

Path 3:

.. image:: ./images/mb_path_3.png

Path 9:

.. image:: ./images/mb_path_9.png

Transition states
~~~~~~~~~~~~~~~~~

We now locate the transition states using the EF method of Wales [D. J. Wales, J. Chem. Phys. **101**, 3750 (1994)].

.. code:: python

    ts1, ts2 = (-0.822021, 0.624313), (0.212487, 0.292988)
    points_dict = {
        (-1.0, 0.3): ts1,
        (-0.7, 1.2): ts1,
        ( 0.0, 0.0): ts2,
        ( 0.5, 0.0): ts2,
    }

    for i, k in enumerate(points_dict):
        db = _test_muller_brown_ef(k[0], k[1], unstable_modes=1)
        coords = numpy.array(db)

        # Make sure all minimizations converged
        ref_coords = points_dict[k]
        assert numpy.all(abs(ref_coords - coords[-1:]) < 0.01)

        # Plot the paths
        fig, ax = plot_mb()
        ax = add_states(ax)
        ax.plot(coords[:,0], coords[:,1], '-', color='orange', label='EF')
        ax.plot(ref_coords[0], ref_coords[1], 'ro')
        fig.savefig(f'images/mb_ts_path_{i}.png')

There are starting point for which the search fails, but in most cases it works well.

.. image:: ./images/mb_ts_path_1.png

.. image:: ./images/mb_ts_path_2.png

Kob-Andersen mixture
--------------------

.. code:: python

    import numpy
    import atooms.trajectory
    import atooms.models
    from atooms.landscape import core
    import matplotlib.pyplot as plt
    import logging
    from atooms.core.utils import Timer

    def main(frame=0):
        interaction = atooms.models.f90.Interaction('kob_andersen')
        for method in ['cg', 'sd', 'sd+cg', 'sd+fire', 'lbfgs', 'fire', 'ef']:
            with atooms.trajectory.Trajectory('../data/ka1.xyz') as th:
                system = th[frame]
                system.interaction = interaction
            t, tsd = Timer(), Timer()
            t.start()
            if method == 'sd+cg':
                res = core.compose(system, [core.steepest_descent, core.conjugate_gradient],
                                   gtol=[1e-3, 1e-10])
            if method == 'sd+fire':
                res = core.compose(system, [core.steepest_descent, core.fire],
                                   kwargs=[{}, dict(dt=1e-2, dtmax=1)],
                                   gtol=[1e-3, 1e-10])
            if method == 'sd':
                res = core.steepest_descent(system)
            if method == 'cg':
                res = core.conjugate_gradient(system)
            if method == 'lbfgs':
                res = core.l_bfgs(system, maxcor=50, gtol=1e-12)
            if method == 'fire':
                res = core.fire(system,  dt=1e-2, dtmax=1)
            if method == 'ef':
                res = core.eigenvector_following(system, unstable_modes=0, trust_scale_down=1.1, trust_scale_up=1.1, trust_radius=0.01, zero_mode=1e-6)
            t.stop()
            print(f'{method:10s}', t, res['function'], res['gradient_norm_square'])

    main(100)

::

    >>>

import numpy
from scipy.optimize import minimize
from atooms.landscape.surfaces import muller_brown

def _test_muller_brown_ef(x0, y0, unstable_modes=-1, freeze_iter=-1):
    from atooms.landscape.methods import ef

    coords = numpy.array([x0, y0])
    db = []
    def store(iteration, coords, *args):
        db.append(coords.copy())    
    res = ef.eigenvector_following(coords,
                                   muller_brown.value,
                                   muller_brown.gradient,
                                   muller_brown.normal_modes,
                                   method='ef', max_iter=20000,
                                   trust_radius=0.025,
                                   trust_fixed=True,
                                   unstable_modes=unstable_modes,
                                   freeze_iter=freeze_iter,
                                   min_trust=1e-7, max_trust=10.0,
                                   callback=store
    )
    return db

def _test_muller_brown_sd(x0, y0):
    from atooms.landscape.methods import sd

    coords = numpy.array([x0, y0])
    db = []
    def store(iteration, coords, *args):
        db.append(coords.copy())
    sd.steepest_descent(coords,
                        muller_brown.value,
                        muller_brown.gradient, dx=1e-5, maxiter=10000000, callback=store)
    return db

def _test_muller_brown_fire(x0, y0):
    from atooms.landscape.methods import fire

    coords = numpy.array([x0, y0])
    db = []
    def store(iteration, coords, *args):
        db.append(coords.copy())
    res = fire(coords,
               muller_brown.value,
               muller_brown.gradient, callback=store)
    return db

def _test_muller_brown_cg(x0, y0):
    coords = numpy.array([x0, y0])
    db = [[x0, y0]]
    def store(coords):
        db.append(coords.copy())
    res = minimize(muller_brown.value, coords, method='CG',
                   jac=muller_brown.gradient,
                   options={'gtol': 1e-10}, callback=store)
    return db

def _test_muller_brown_cg_sd(x0, y0, sd_iter=10, sd_dx=1e-4):
    from atooms.landscape.methods import sd

    coords = numpy.array([x0, y0])
    db = [[x0, y0]]
    def store(coords):
        db.append(coords.copy())
    def store_sd(iteration, coords, *args):
        db.append(coords.copy())        
    sd.steepest_descent(coords,
                        muller_brown.value,
                        muller_brown.gradient, dx=sd_dx, gtol=1e3, callback=store_sd)
    res = minimize(muller_brown.value, coords, method='CG',
                   jac=muller_brown.gradient,
                   options={'gtol': 1e-10}, callback=store)
    return db

def _test_muller_brown_sd_fire(x0, y0):
    from atooms.landscape.methods import sd
    from atooms.landscape.methods import fire

    coords = numpy.array([x0, y0])
    db = [[x0, y0]]
    def store(iteration, coords, *args):
        db.append(coords.copy())        
    sd.steepest_descent(coords,
                        muller_brown.value,
                        muller_brown.gradient, dx=1e-4, gtol=1e4, callback=store)
    res = fire(coords,
               muller_brown.value,
               muller_brown.gradient, callback=store)
    return db


def _test_muller_brown_lbfgs(x0, y0):
    coords = numpy.array([x0, y0])
    db = [[x0, y0]]
    def store(coords):
        db.append(coords.copy())
    res = minimize(muller_brown.value, coords, method='L-BFGS-B',
                   jac=muller_brown.gradient,
                   options={'gtol': 1e-10, 'maxcor': 10}, callback=store)
    return db

import numpy
from atooms.landscape.surfaces import muller_brown

def plot_mb():
    import numpy as np
    import matplotlib.pyplot as plt

    def create_mesh(f):
        from itertools import product
        x = np.arange(-1.5, 1, 0.025)
        y = np.arange(-0.5, 2, 0.025)
        X, Y = np.meshgrid(x, y)
        Z = np.zeros(X.shape)
        mesh_size = range(len(X))
        for i, j in product(mesh_size, mesh_size):
            Z[i][j] = f(np.array([X[i][j], Y[i][j]]))
        return X, Y, Z

    def plot_contour(ax, X, Y, Z):
        ax.contour(X, Y, Z, levels=np.linspace(-140, 100, 20))
        ax.axis('square')
        return ax

    fig, ax = plt.subplots(figsize=(6, 6), frameon=False)
    X, Y, Z = create_mesh(muller_brown.value)
    ax = plot_contour(ax, X, Y, Z)

    return fig, ax

def add_states(ax):
    """Add transition states and minima to plot"""
    props = dict(alpha=.5, facecolor='white', lw=0.0)
    ax.annotate('M1', (0.623499, 0.028038), bbox=props)
    ax.annotate('M2', (-0.558224, 1.441726), bbox=props)
    ax.annotate('M3', (-0.050011, 0.466694), bbox=props)
    ax.annotate('TS1', (-0.822021, 0.624313), bbox=props)
    ax.annotate('TS2', (0.212487, 0.292988), bbox=props)
    return ax
def test_muller_brown():
    # See D. J. Wales, J. Chem. Phys. 101, 3750 (1994)
    min1, min2, min3 = (0.623499, 0.028038), (-0.558224, 1.441726), (-0.050011, 0.466694)
    points_dict = {
        (-0.5, 0.75): min3,  # 3
        (-0.6, 0.9): min2,  # 4
        (-0.05, -0.3): min1,  # 6
        (-0.5, -0.3): min3,  # 7
        (-1.3, 1.6): min2,  # 8
        (0.9, 0.7): min1,  # 10
        (0.5, 1.7): min2,  # 11
        (0.8, 1.1): min3,  # 12
        (0.7, 1.3): min3,  # 13
        (0.6, 1.55): min2,  # 14
        (-1.0, -0.3): min3,  # 15
        (-1.3, -0.1): min2,  # 16
    }
    # Paths reaching the minimum (value) from a starting point (key)
    for i, k in enumerate(points_dict):
        fig, ax = plot_mb()
        ax = add_states(ax)
        ref_coords = points_dict[k]

        db = _test_muller_brown_ef(k[0], k[1], unstable_modes=0)
        xs = numpy.array(db)
        ax.plot(xs[:,0], xs[:,1], '-', color='orange', label='EF')
        ax.plot(ref_coords[0], ref_coords[1], 'ro')

        db = _test_muller_brown_sd(k[0], k[1])
        xs = numpy.array(db)
        ax.plot(xs[:,0], xs[:,1], '-', color='red', label='SD')

        db = _test_muller_brown_cg(k[0], k[1])
        xs = numpy.array(db)
        ax.plot(xs[:,0], xs[:,1], linestyle='--', marker='o', color='blue', label='CG')

        db = _test_muller_brown_sd_fire(k[0], k[1])
        xs = numpy.array(db)
        ax.plot(xs[:,0], xs[:,1], linestyle='--', marker='o', color='cyan', label='SD+FIRE')

        db = _test_muller_brown_fire(k[0], k[1])
        xs = numpy.array(db)
        ax.plot(xs[:,0], xs[:,1], '-', color='purple', label='FIRE')
        
        fig.legend()
        fig.savefig(f'images/mb_path_{i}.png')
            
test_muller_brown()

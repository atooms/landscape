def ef(coords, trust_radius=0.2, trust_fixed=False,
       trust_dynamic=False, method='ef', freeze_iter=-1,
       freeze_unstable_modes=-1):
    import numpy
    from atooms.energy_landscape.surfaces import muller_brown
    from atooms.energy_landscape.methods import eigenvector_following

    if not trust_dynamic and not trust_fixed:
	trust_dynamic=True
    coords = numpy.array([float(_) for _ in coords.split(',')])
    eigenvector_following(coords,
			  function=muller_brown.value,
			  gradient=muller_brown.gradient,
			  normal_modes=muller_brown.normal_modes,
			  method=method,
			  maxiter=2000,
			  trust_radius=trust_radius,
			  trust_fixed=trust_fixed,
			  freeze_iter=freeze_iter,
                          freeze_unstable_modes=freeze_unstable_modes,
			  dump_coords=True,
			  max_trust=10.0,
			  min_trust=1e-7
    )

def sd(coords, dx=0.00001):
    import numpy
    from atooms.energy_landscape.backends import muller_brown
    from atooms.energy_landscape.sd import steepest_descent

    coords = numpy.array([float(_) for _ in coords.split(',')])
    steepest_descent(coords,
		     function=muller_brown.value,
		     gradient=muller_brown.gradient,
		     maxiter=2000,
		     dump_coords=True,
		     dx=dx
    )

if __name__ == '__main__':
    from argh import dispatch_commands
    dispatch_commands([ef, sd])

#!/usr/bin/env python

import os
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

with open('README.md') as f:
    readme = f.read()

setup(name='atooms-landscape',
      version='2.0.1',
      description='Energy landscape analysis tools built with atooms',
      long_description=readme,
      long_description_content_type="text/markdown",
      author='Daniele Coslovich',
      author_email='daniele.coslovich@umontpellier.fr',
      url='https://framagit.org/atooms/landscape',
      packages=['atooms', 'atooms/landscape', 'atooms/landscape/surfaces',
                'atooms/landscape/methods'],
      license='GPLv3',
      install_requires=['atooms>=3.20.1', 'f2py-jit>=0.5.0', 'scipy'],
      zip_safe=False,
      classifiers=[
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.6',
      ]
     )
